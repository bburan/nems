#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Jun 12 16:36:10 2017

@author: shofer
"""

#Initialization file for the NEMS function package. 

"""
This package currently has the following modules available for fitting:
    1. FIR.py
    2. input_log.py
    3. pupil_gain.py
    4. no_pupil_gain.py
    5. DEXP.py
    6. tanhsig.py
    7. gauss_nl.py
"""