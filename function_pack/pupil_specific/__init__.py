#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Jun 13 17:42:28 2017

@author: shofer
"""

#Initialization file for pupil specific package within the function package

#Theres not too much in this package, just a couple functions that can sort
#pupil data in neat ways, though these are also probably applicable to other 
#continuous state variables as well.