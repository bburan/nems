#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Jun 12 14:10:51 2017

@author: shofer
"""
##Initialization file for the Plotting Package for NEMS

#The plotting functions in this package should be able to plot both a FERReT
#object and "loose" data from a trial (i.e. raw trial data, not associated to
#a specific FERReT object). Currently implemented are:
    #   1. raster_plot.py: raster plots of response
    #   2. coeff_heatmap.py: heatmap of model coefficients
    #   3. comparison.py: plots to compare prediction vs response
